

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
void main(){
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Center(

    child: Column(
    children: [
    SizedBox(
    height: 160,
  ),
  Icon(
  Icons.send_to_mobile,
  color: Colors.tealAccent[700],
  size: 150.0,
  semanticLabel: 'Text to announce in accessibility modes',
  ),
  SizedBox(
  height: 15,
  ),
  Text("Verification Code",
  style: TextStyle(
  backgroundColor: Colors.white,
  fontSize: 25,
  fontWeight: FontWeight.bold,


  ),
  ),
  SizedBox(
  height: 15,
  ),
  Text("OTP has been send to you on your \nmobile number,please enter it below",
  style: TextStyle(
  color: Colors.grey,
  backgroundColor: Colors.white,
  fontSize: 14,


  ),
  ),
  PinEntryTextField(
  fields: 5,
  onSubmit: (String pin){
  showDialog(
  context: context,
  builder: (context){
  return AlertDialog(
  title: Text("Pin"),
  content: Text('Pin entered is $pin'),
  );
  }
  ); //end showDialog()

  }, // end onSubmit
  ),
      SizedBox(
        height: 20,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Didnt get the OTP?',
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.grey,
                fontStyle: FontStyle.italic),
          ),
          InkWell(
            child: Text(
              ' Sign',
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.tealAccent[700],
                  fontStyle: FontStyle.italic),
            ),
            onTap: () {

            },
          ),
        ],
      ),
      SizedBox(
        height: 20,
      ),
      FlatButton(

        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
            side: BorderSide(color: Colors.greenAccent)),
        child: Text("Verify &Proceed"),
        color: Colors.blue,
        textColor: Colors.white,
      ),

    ],
    ),
    ),
  );}
}