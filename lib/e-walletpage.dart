import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Ewallet(),
  ));
}

class Ewallet extends StatefulWidget {
  const Ewallet({Key key}) : super(key: key);

  @override
  _EwalletState createState() => _EwalletState();
}

class _EwalletState extends State<Ewallet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            height: 250,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.tealAccent[700],
              boxShadow: [
                BoxShadow(color: Colors.grey),
              ],
              borderRadius: BorderRadius.vertical(
                bottom:
                    Radius.elliptical(MediaQuery.of(context).size.width, 250.0),
              ),
            ),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  "e-Wallet",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ]),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "e-Wallet Ballance",
                  style: TextStyle(fontSize: 20),
                )),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 18.0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Rs. 1245.25",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                )),
          ),
        ]),
      ),
    );
  }
}
